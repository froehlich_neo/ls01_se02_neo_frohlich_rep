
public class TemperaturTabelle {
	public static void main(String[] args) {

		System.out.printf("%s", "Fahrenheit");
		System.out.printf("%3s", "|");
		System.out.printf("%10s", "Celsius");
		System.out.println("");
		System.out.println("-----------------------");
		System.out.printf("%s", "-20");
		System.out.printf("%10s", "|");
		System.out.printf("%10.2f", -28.8889);
		System.out.println("");
		System.out.printf("%s", "-10");
		System.out.printf("%10s", "|");
		System.out.printf("%10.2f", -23.3333);
		System.out.printf("");
		System.out.printf("%s", "+0");
		System.out.printf("%11s", "|");
		System.out.printf("%10.2f", -17.7778);
		System.out.printf("");
		System.out.printf("%s", "+20");
		System.out.printf("%10s", "|");
		System.out.printf("%10.2f", -6.66667);
		System.out.printf("");
		System.out.printf("%s", "+30");
		System.out.printf("%10s", "|");
		System.out.printf("%s", "Fahrenheit");
		System.out.printf("%10.2f", -1.1111);
		

	}
}
